// Récupère le div avec l'id "toggle"
const toggleSwitch = document.querySelector('#toggle');
// Récupère l'élément body
const body = document.querySelector('body');
// Récupère la search bar
const searchBar = document.querySelector('#searchBar');
// Récupère l'input de la search bar
const searchBarInput = document.querySelector('#searchInput');
// On récupère toutes les balises de liens
const elementA = $("a");
// On récupère les différents éléments de la page
const main = $("main");
main.hide();
// On récupère la popUp
const popup = $("#popUp");
popup.hide();
const noResults = $("#noResult");
noResults.hide();
const popUpShadow = document.getElementById("popUp");

// Fonction qui se déclenche au changement de notre variable qui point vers le toggle-slider
toggleSwitch.addEventListener('change', function() {
    //Si le toggle est checké, alors on passe la page en dark mode en ajoutant à Body la classe "dark"
    if (this.checked) {
        body.classList.add('dark');
        // Modifications de la barre de recherche
        searchBar.style.backgroundColor = '#1f1f1f';
        searchBarInput.style.color = 'white';
        popup.get(0).style.backgroundColor = "#1F1F1F";
        popUpShadow.style.boxShadow = "0px 5px 30px #A445ED";
        elementA.style.color = "white";
        
    }
    //Quand le toggle n'est pas activé, on enlève la classe du dark mode
    else {
        body.classList.remove('dark');
        // Modifications de la barre de recherche
        searchBar.style.backgroundColor = '#CDCDCD';
        searchBarInput.style.color = '#2D2D2D';
        popup.get(0).style.backgroundColor = "white";
        popUpShadow.style.boxShadow = "0px 5px 30px rgba(0, 0, 0, 0.1)";
        elementA.style.color = "white";
        
    }
}
);

// Instantiation de la variable JSON 
let result;
// On récupère le titre de la page
const title = $("#dicTitleLeft>h1");
// On récupère la partie de la phonétique
const phonetic = $("#dicTitleLeft>h2");
// On récupère la balise qui s'occupe de l'audio
const audio = $("#audio");
// On récupère le parent des "li" de meanings pour les créer en chaîne
const meaningsList = $("#meaningsList");
// On récupère le parent des "li" de synonyms
const synonymList = $("#synonym");
// On récupère le parent de l'audio
const audioParent = $("#dicTitleRight");
// On initie l'audio à une valeur null
let newAudio;
// ON récupère la partie verb
const verbose = $("#verb>ul");
// On récupère la partie wiki
const wikiText = $("#wiki>a>p");
// On récupère l'image du wiki
const wikiTextLink = $(".wikiLinkUrl");
// On cache le message d'erreur
$("#errorMessage").hide();
// Quand on clique sur la loupe, cela crée l'appel de l'API pour ajouter du contenu
$("#iconSearch").click(function(){
  // On récupère la valeur contenue à l'intérieur du champs
  let inputVal = $("#searchInput").val();
  if (inputVal != ""){
    jsonUrl = "https://api.dictionaryapi.dev/api/v2/entries/en/" + inputVal;
    $.getJSON(jsonUrl).done(function(result){
      $("#errorMessage").hide();
      $("#searchBar").css('border', 'none');
      console.log(result[0]);
      // Header part filled
      title.text(result[0].word);
      phonetic.text(result[0].phonetics[0].text);

      // Création et indexation de l'audio
      newAudio = audio.attr('src', result[0].phonetics[0].audio);
      audioParent.append(newAudio);
      
      // Code permettant de supprimer tous les "li" à chaque nouvelle requête
      if (meaningsList.find("li")){
        meaningsList.find("li").remove();
      }
      if (synonymList.find("li")){
        synonymList.find("li").remove();
      }
      if (verbose.find("li")){
        verbose.find("li").remove();
      }

      // Code pour récupérer toutes les définitions
      meanings = result[0].meanings[0].definitions;
      meanings.forEach(meaning => {
        let newMeaningLi = $("<li>").text(meaning.definition);
        meaningsList.append(newMeaningLi);
      });

      // Code pour récupérer les synonymes
      synonyms = result[0].meanings[0].synonyms;
      synonyms.forEach(synonym => {
        let newMeaningLi = $("<li>").text(synonym);
        synonymList.append(newMeaningLi);
      });

      // Partie Verbose
      verbs = result[0].meanings[1].definitions;
      verbs.forEach(verb => {
        let verboseLi = $("<li>").text(verb.definition);
        verbose.append(verboseLi);
      });

      //Partie Source
      let wikiUrl = result[0].sourceUrls;
      wikiText.text(wikiUrl);
      wikiTextLink.attr('href', wikiUrl);

      // On montre le main
      main.show();
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      if (jqXHR.status == 404) {
        main.hide();
        noResults.show();
      } else {
        main.hide();
        noResults.show();
      }
    });
  }
  else{
    // Fonction rajoutant des éléments dans la page si le JSON renvoyé est null
    console.log("C'EST VIDE WESH");
    $("#searchBar").css('border', '1px solid #FF5252');
    $("#errorMessage").show();
  }
});

// Fonction permettant de faire jouer l'audio PROBLEM ACTUELS
function playAudio() {
  newAudio.get(0).play();
}

// Fonction permettant de sélectionner la police d'écriture
function fontSelector(){
  if (popup.is(":hidden")){
    popup.show();
  }
  else{
    popup.hide();
  }
}

//Fonctions permettant de changer la police d'écriture
function fontSerif(){
  $('h1').css('font-family', 'Lora Bold');
  $('h2').css('font-family', 'Lora');
  $('h3').css('font-family', 'Lora Bold');
  $('h4').css('font-family', 'Lora');
  $('li').css('font-family', 'Lora');
  $('p').css('font-family', 'Lora');
}

function fontSansSerif(){
  $('h1').css('font-family', 'Inter Bold');
  $('h2').css('font-family', 'Inter');
  $('h3').css('font-family', 'Inter Bold');
  $('h4').css('font-family', 'Inter');
  $('li').css('font-family', 'Inter');
  $('p').css('font-family', 'Inter');
}

function fontMono(){
  $('h1').css('font-family', 'Inconsolata Bold');
  $('h2').css('font-family', 'Inconsolata');
  $('h3').css('font-family', 'Inconsolata Bold');
  $('h4').css('font-family', 'Inconsolata');
  $('li').css('font-family', 'Inconsolata');
  $('p').css('font-family', 'Inconsolata');
}

// Fonction déterminant si la barre de recherche est vide ou non
